package Chapter3.Ch3_18;

// Типичный пример использования открытого статического класса-члена
public class Calculator {
    public static abstract class Operation {
        private final String name;

        Operation(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }

        // Выполняет арифметическую операцию, представленную данной константой
        abstract double eval(double х, double у);

        // Дважды вложенные анонимные классы
        public static final Operation PLUS = new Operation("+") {
            double eval(double х, double у) {
                return х + у;
            }
        };

        public static final Operation MINUS = new Operation("-") {
            double eval(double x, double y) {
                return x - y;
            }
        };

        public static final Operation TIMES = new Operation("*") {
            double eval(double х, double у) {
                return х * у;
            }
        };

        public static final Operation DIVIDE = new Operation("/") {
            double eval(double х, double у) {
                return х / у;
            }
        };
    }

    // Возвращает результат указанной операции
    public double calculate(double x, Operation op, double у) {
        return op.eval(x, у);
    }
}
