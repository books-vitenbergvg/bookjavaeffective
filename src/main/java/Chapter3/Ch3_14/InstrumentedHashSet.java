package Chapter3.Ch3_14;

import java.util.Collection;
import java.util.HashSet;

// Ошибка: неправильное использование наследования!
public class InstrumentedHashSet extends HashSet {
    // Число попыток вставить элемент
    private int addCount = 0;

    public InstrumentedHashSet() {
    }

    public InstrumentedHashSet(Collection c) {
        super(c);
    }

    public InstrumentedHashSet(int initCap, float loadFactor) {
        super(initCap, loadFactor);
    }

    public boolean add(Object o) {
        addCount++;
        return super.add(o);
    }

    public boolean addAll(Collection c) {
        addCount += c.size();
        return super.addAll(c);
    }

    public int getAddCount() {
        return addCount;
    }
}

