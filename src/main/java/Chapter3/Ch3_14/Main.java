package Chapter3.Ch3_14;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    //1. Мы предполагаем, что метод getAddCount должен возвратить число 3, но он возвращает 6.
    //Внутри класса HashSet метод addAll реализован поверх его метода add, хотя в документации эта деталь реализации не отражена.
//    InstrumentedHashSet s = new InstrumentedHashSet();
//    s.addAll(Arrays.asList(new String[] {"Snap", "Crackle", "Рор"}));
//
//    //2. данный класс можно применять для расширения возможностей реализации интерфейса Set, он будет работать с любым предоставленным ему конструктором
//    Set s1 = new InstrumentedSet(new TreeSet(list));
//    Set s2 = new InstrumentedSet(new HashSet(capacity, loadFactor));
}
