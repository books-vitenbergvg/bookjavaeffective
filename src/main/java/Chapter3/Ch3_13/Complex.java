package Chapter3.Ch3_13;

public final class Complex {
    private final float re;
    private final float im;

    public Complex(float re, float im) {
        this.re = re;
        this.im = im;
    }

    //Методы доступа без соответствующих мутаторов
    public float realPart() {
        return re;
    }

    public float imaginaryPart() {
        return im;
    }

    public Complex add(Complex c) {
        return new Complex(re + c.re, im + c.im);
    }

    public Complex substract(Complex c) {
        return new Complex(re - c.re, im - c.im);
    }

    public Complex multiply(Complex c) {
        return new Complex(re * c.re - im * c.im, re * c.im + im * c.re);
    }

    public Complex divide(Complex c) {
        float tmp = c.re * c.re + c.im * c.im;
        return new Complex((re * c.re + im * c.im) / tmp, (im * c.re - re * c.im) / tmp);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Complex)) {
            return false;
        }
        Complex c = (Complex) obj;
        return (Float.floatToIntBits(re) == Float.floatToIntBits(c.re)) && (Float.floatToIntBits(im) == Float.floatToIntBits(c.im));
    }

    @Override
    public int hashCode() {
        int result = 17 + Float.floatToIntBits(re);
        result = 37 * result + Float.floatToIntBits(im);
        return result;
    }

    @Override
    public String toString() {
        return "(" + re + " + " + im + "i)";
    }
}

    //Неизменяемый класс со статическими методами генерации вместо конструкторов
//    public class Complex {
//        private final float re;
//        private final float im;
//
//        private Complex(float re, float im) {
//            this.re = re;
//            this.im = im;
//        }
//
//        public static Complex valueOf(float re, float im){
//            return new Complex(re, im);
//        }
//        //...Остальное не изменилось
//    }
