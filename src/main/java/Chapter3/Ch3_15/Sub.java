package Chapter3.Ch3_15;

import java.util.Date;

//Предполагается, что эта про грамма напечатает текущую дату дважды, но в первый раз она выводит null,
//поскольку метод m вызывается конструктором Super() прежде, чем конструктор Sub() получает возможность инициализировать поле даты.
public class Sub extends Super {
    private final Date date;

    // Пустое поле final заполняется конструктором
    Sub() {
        date = new Date();
    }

    // Переопределяет метод Supeг.m, используемый конструктором Super()
    public void m() {
        System.out.println(date);
    }

    public static void main(String[] aгgs) {
        Sub s = new Sub();
        s.m();
    }
}
