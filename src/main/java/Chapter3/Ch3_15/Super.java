package Chapter3.Ch3_15;

public class Super {
    // Ошибка: конструктор вызывает переопределяемый метод
    public Super() { m();}
    public void m() { }
}