package Chapter2.Ch2_8;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map m = new HashMap();
        m.put(new PhoneNumber(408, 867, 5309), "Jenny");

        m.get(new PhoneNumber(408, 867, 5309)); //Ожидаем что возвратит "Jenny" - возвратит null
    }
}
