package Chapter2.Ch2_9;

public class PhoneNumber {
    private final short areaCode;
    private final short exchange;
    private final short extension;

    public PhoneNumber(int areaCode, int exchange, int extension) {
        rangeCheck(areaCode, 999, "area code");
        rangeCheck(exchange, 999, "exchange");
        rangeCheck(extension, 9999, "extention");
        this.areaCode = (short) areaCode;
        this.exchange = (short) exchange;
        this.extension = (short) extension;
    }

    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max) {
            throw new IllegalArgumentException(name + ": " + arg);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PhoneNumber))
            return false;
        PhoneNumber pn = (PhoneNumber) o;
        return pn.extension == extension &&
                pn.exchange == exchange &&
                pn.areaCode == areaCode;
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + areaCode;
        result = 37 * result + exchange;
        result = 37 * result + extension;
        return result;
    }


    //Возвращает представление данного телефонного номера в виде строки.
    //Строка состоит из четырнадцати символов, имеющих формат
    //"(ХХХ) YYY-ZZZZ" , где ХХХ - код зоны, У У У - номер АТС,
    //ZZZZ - номер абонента в АТС.
    //Если какая-либо из трех частей телефонного номера мала и
    //не заполняет свое поле, последнее дополняется ведущими нулями.
    public String toString() {
        return "(" + toPaddedString(areaCode, 3) + ")" +
                toPaddedString(exchange, 3) + "-" +
                toPaddedString(extension, 4);
    }

    //Преобразует значение типа int в строку указанной длины, дополненную ведущими нулями
    private static String toPaddedString(int i, int length) {
        String s = Integer.toString(i);
        return ZEROS[length - s.length()] + s;
    }

    private static String[] ZEROS =
            {"", "0", "00", "000", "0000",
                    "00000", "000000", "0000000", "00000000", "000000000"
            };
}
