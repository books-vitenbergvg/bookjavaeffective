package Chapter2.Ch2_7;

//Строка без учёта регистра. Регистр исходной строки сохраняется
// методом toString, однако при сравнениях игнорируется
public final class CaseInsensitiveString {
    private String s;

    public CaseInsensitiveString(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        this.s = s;
    }

    //Ошибка - нарушение симметрии!
    @Override
    public boolean equals(Object o) {
        if (o instanceof CaseInsensitiveString) {
            return s.equalsIgnoreCase(
                    ((CaseInsensitiveString)o).s);
        }

        //Одностороннее взаимодейсствие!
        if (o instanceof String) {
            return s.equalsIgnoreCase((String)o);
        }
        return false;
    }
    //----------

    //Исправленный метод
//    public boolean equals(Object o) {
//        return o instanceof CaseInsensitiveString &&
//                ((CaseInsensitiveString)o).s.equalsIgnoreCase(s);
//    }
}
