package Chapter2.Ch2_7;

public class Main {
    public static void main(String[] args) {
        CaseInsensitiveString cis = new CaseInsensitiveString("Text");
        String s = "text";
        cis.equals(s);//true
        s.equals(cis);//false(equals класса String не указывает равенство для разных регистров)
    }
}
