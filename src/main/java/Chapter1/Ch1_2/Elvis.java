package Chapter1.Ch1_2;

public class Elvis {
    //Вариант 1 - открытый статический член является полем типа final
//    public static final Elvis INSTANCE = new Elvis();
//
//    private Elvis() {
//        //...
//    }

    //Вариант 2 - Синглтон со статическим методом генерации
    private static final Elvis INSTANCE = new Elvis();

    private Elvis() {
        //...
    }

    public static Elvis getInstance() {
        return INSTANCE;
    }
}
