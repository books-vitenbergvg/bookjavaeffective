package Chapter1.Ch1_1;

import java.util.HashMap;
import java.util.Map;

//Эскиз системы с предоставлением услуг
public abstract class AnySystem {
    //Ставим ключ типа String в соответствие объекту Class
    private static Map implementations = null;

    //При первом вызове инициализирует карту соответствия
    private static synchronized void initMapIfNecessary() {
        if (implementations == null) {
            implementations = new HashMap();
            //Загружает названия классов и ключи из файла Properties
            //Транслирует названия в объекты Class, используя Class.forName и сохраняет их соответствие ключам
        }
    }

//    public static AnySystem getInstance(String key) {
//        initMapIfNecessary();
//        Class c = (Class) implementations.get(key);
//        if (c == null) {
//            return new DefaultAnySystem();
//        }
//        try {
//            return (AnySystem) c.newInstance();
//        }
//        catch (Exception e) {
//            return new DefaultAnySystem();
//        }
//    }
}
