package TODO11;

//A: alphonse.bow(gaston) — получает лок alphonse;
//A: gaston.bowBack(alphonse) — получает лок gaston;
//A: возвращается из обоих методов, тем самым освобождая лок.
//В это же время:
//G: gaston.bow(alphonse) — получает лок gaston;
//G: alphonse.bowBack(gaston) — получает лок alphonse;
//G: возвращается из обоих методов, тем самым освобождая лок.

//Пример дедлока:
//A: alphonse.bow(gaston) — получает лок alphonse
//G: gaston.bow(alphonse) — получает лок gaston
//G: пытается вызвать alphonse.bowBack(gaston), но блокируется, ожидая лока alphonse
//A: пытается вызвать gaston.bowBack(alphonse), но блокируется, ожидая лока gaston

public class Deadlock {
    static class Friend {
        private final String name;

        public Friend(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public synchronized void bow(Friend bower) {
            System.out.format("%s: %s" + "  has bowed to me!%n", this.name, bower.getName());
            bower.bowBack(this);
        }

        public synchronized void bowBack(Friend bower) {
            System.out.format("%s: %s"
                            + " has bowed back to me!%n",
                    this.name, bower.getName());
        }
    }

    public static void main(String[] args) {
        final Friend alphonse =
                new Friend("Alphonse");
        final Friend gaston =
                new Friend("Gaston");
        new Thread(new Runnable() {
            @Override
            public void run() {
                // System.out.println("Thread 1");
                alphonse.bow(gaston);
                // System.out.println("Th: gaston bowed to alphonse");
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                //  System.out.println("Thread 2");
                gaston.bow(alphonse);
                //  System.out.println("2.gaston waiting alph bowed");
            }
        }).start();
    }
}
