package Chapter4.Ch4_21;

//Перечисление, использующее порядковые номера
public class SuitWithNumbers implements Comparable {
    private final String name;

    //Порядковый номер следующей масти
    private static int nextOrdinal = 0;

    //Назначение порядкового номера
    private final int ordinal = nextOrdinal++;

    private SuitWithNumbers(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public int compareTo(Object o) {
        return ordinal - ((SuitWithNumbers) o).ordinal;
    }

    public static final SuitWithNumbers CLUBS = new SuitWithNumbers("clubs");//трефы
    public static final SuitWithNumbers DIAMONDS = new SuitWithNumbers("diamonds");//бубны
    public static final SuitWithNumbers HEARTS = new SuitWithNumbers("hearts");//черви
    public static final SuitWithNumbers SPADES = new SuitWithNumbers("spades");//пики
}
