package Chapter4.Ch4_21;

//шаблон перечисления типов - typesafe enum
public class Suit {
    private final String name;

    private Suit(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public static final Suit CLUBS = new Suit("clubs");//трефы
    public static final Suit DIAMONDS = new Suit("diamonds");//бубны
    public static final Suit HEARTS = new Suit("hearts");//черви
    public static final Suit SPADES = new Suit("spades");//пики
}
