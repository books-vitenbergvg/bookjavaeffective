package Chapter4.Ch4_19;

//Вырожденные классы, подобные этому не должны быть открытыми!
class Point {
    public float x;
    public float y;
}

//Замена класса Point классом с закрытыми методами доступа

//Класс с инкапсулированной структурой
class PointIncaps {
    private float x;
    private float y;

    public PointIncaps(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
}
