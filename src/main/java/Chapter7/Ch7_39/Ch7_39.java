package Chapter7.Ch7_39;

public class Ch7_39 {
    // Неправильное использование исключений. Никогда так не делайте!
    public static void main(String[] args) {
        int a[] = new int[10];
        try {
            int i = 0;
            //while (true) a[i++].f();
        } catch (ArrayIndexOutOfBoundsException е) {
        }

        //Нормальный вариант:
        for (int i = 0; i < a.length; i++) {
            //a[i].f();
        }
    }
}
