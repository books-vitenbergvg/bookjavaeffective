package Chapter7.Ch7_43;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class Ch7_43 {
    /**
     * Возвращает элемент, находящийся в указанной позиции
     * в заданном списке.
     *
     * @throws IndexOutOfBoundsException, если индекс находится
     *                                    за пределами диапазона (index < 0 II index >= size()).
     */

    //В этом примере трансляция исключения продиктована спецификацией метода get в интерфейсе List:
//    public Object get(int index) {
//        ListIterator i = listIterator(index);
//        try {
//            return i.next();
//        } catch (NoSuchElementException e) {
//            throw new IndexOutOfBoundsException("Index: " + index);
//        }
//    }
}
