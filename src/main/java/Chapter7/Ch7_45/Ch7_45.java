package Chapter7.Ch7_45;

public class Ch7_45 {
    /**
     * Конструируем IndexOutOfBoundsException
     *
     * @param upperBound – самое большее из разрешенных значений индекса плюс один
     * @раram lowerBound – самое меньшее из разрешенных значений индекса
     * @раrаm index  – действительное значение индекса
     */

    //Вместо конструктора String исключение IndexOutOfBounds могло бы иметь следующий конструктор
//    public IndexOutOfBoundsException(int lowerBound, int upperBound, int index) {
//        // Генерируем описание исключения,
//        // фиксирующее обстоятельства отказа
//        super("Lower bound: " + lowerBound + ", Upper bound: " + upperBound + ", Index: " + index);
//    }

}
