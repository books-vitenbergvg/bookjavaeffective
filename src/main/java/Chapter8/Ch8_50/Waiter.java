package Chapter8.Ch8_50;

import java.util.LinkedList;

public class Waiter {
    //Стандартная схема использования метода wait:
    public static void main(String[] args) {
        boolean condition = true;
        Object obj = new LinkedList();

        synchronized (obj) {
            while (condition) {
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // Выполнение действия, соответствующего условию
        }
    }
}
