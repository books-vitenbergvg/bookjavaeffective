package Chapter8.Ch8_48;

// Ошибка: требуется синхронизация

//При отсутствии синхронизации нет гарантии, что поток, подлежащий остановке,
//увидит, что другой поток поменял значение stopRequested
public class StoppableThread extends Thread {
    private boolean stopRequested = false;

    public void run() {
        boolean dоnе = false;
        while (!stopRequested && !dоnе) {
            // Здесь выполняется необходимая обработка
        }
    }

    public void requestStop() {
        stopRequested = true;
    }


    //Правильно синхронизированное совместное завершение потока
    public synchronized void goodRun() {
        boolean dоnе = false;
        while (!stopRequested && !dоnе) {
            // Здесь выполняется необходимая обработка
        }
    }

    public synchronized void goodRequestStop() {
        stopRequested = true;
    }

    //Добавляем:
    private synchronized boolean stopRequested() {
        return stopRequested;
    }


    //Синхронизацию можно опустить, если объявить stopRequested с модификатором volatile (асинхронно-изменяемый).
    //Этот модификатор гарантирует, что любой поток, который будет читать это поле,
    //увидит самое последнее записанное значение.
}
