package Chapter8.Ch8_48;

public class DoubleCheck {
    // Двойная проверка отложенной инициализации – неправильная!

    //Нет гарантий, что ссылка на объект будет работать правильно.
    //Если поток прочел ссылку на объект без синхронизации, а затем вызывает в
    //этом объекте какой-либо метод, может оказаться, что метод обнаружит свой объект в частично
    //инициализированном состоянии, что приведет к катастрофическому сбою программы.
    private static Foo foo = null;

    public static Foo getFoo() {
        if (foo == null) {
            synchronized (Foo.class) {
                if (foo == null)
                    foo = new Foo();
            }
        }
        return foo;
    }


    //Способ решения:
    //Нормальная статическая инициализация (неотложенная)
    private static final Foo goodFoo = new Foo();

    public static Foo getGoodFoo() {
        return goodFoo;
    }


}

