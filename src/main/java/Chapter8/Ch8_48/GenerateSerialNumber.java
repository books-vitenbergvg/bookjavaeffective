package Chapter8.Ch8_48;

//Задача - создать генератор серийных номеров, который будет генерировать неповторяющиеся значения.
public class GenerateSerialNumber {
    static int nextSerialNumber = 0;
    // Ошибка: требуется синхронизация private static iпt

    //Оператор приращения осуществляет чтение и запись в поле nextSerialNumber, а потому
    //атомарным не является. Чтение и запись - независимые операции, которые
    //выполняются последовательно. Несколько параллельных потоков могут наблюдать в поле
    //nextSerialNumber одно и то же значение и возвращать один и тот же серийный номер.
    //Также, без синхронизации второй поток может не увидеть ни одного из изменений, произведенных первым потоком.
    public static int generateSerialNumber() {
        return nextSerialNumber++;
    }

    public synchronized static int goodGenerateSerialNumber() {
        return nextSerialNumber++;
    }
}
