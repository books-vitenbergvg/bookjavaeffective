package Chapter8.Ch8_49;

import java.util.LinkedList;
import java.util.List;

public abstract class WorkQueue {
    private final List queue = new LinkedList();
    private boolean stopped = false;

    protected WorkQueue() {
        new WorkerThread().start();
    }

    public final void enqueue(Object workItem) {
        synchronized (queue) {
            queue.add(workItem);
            queue.notify();
        }
    }

    public final void stop() {
        synchronized (queue) {
            stopped = true;
            queue.notify();
        }
    }

    protected abstract void processItem(Object workltem)
            throws InterruptedException;

    // Ошибка: вызов чужого метода из синхронизированного блока!
    private class WorkerThread extends Thread {
        public void run() {
            while (true) { //Главный цикл
                synchronized (queue) {
                    try {
                        while (queue.isEmpty() && !stopped)
                            queue.wait();
                    } catch (InterruptedException е) {
                        return;
                    }
                    if (stopped)
                        return;
                    Object workItem = queue.remove(0);
                    try {
                        processItem(workItem); // Блокировка!
                    } catch (InterruptedException е) {
                        return;
                    }
                }
            }
        }
    }
}

class DeadlockQueue extends WorkQueue {
    protected void processltem(final Object workltem)
            throws InterruptedException {
        //Создаем новый поток, который возвращает workltem в очередь
        Thread child = new Thread() {
            public void run() {
                enqueue(workltem);
            }
        };
        child.start();
        child.join(); //Взаимная блокировка!
    }

    protected void processItem(Object workltem) throws InterruptedException {

    }


    //РЕШЕНИЕ:
    //Чужой метод за пределами синхронизированного блока "открытый вызов"
    private class WorkerThread extends Thread {
        private final List queue = new LinkedList();
        private boolean stopped = false;

        public void run() {
            while (true) { // Главный цикл
                Object workItem = null;
                synchronized (queue) {
                    try {
                        while (queue.isEmpty() && !stopped)
                            queue.wait();
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (stopped)
                        return;
                    workItem = queue.remove(0);
                }
                try {
                    processItem(workItem); // Блокировки нет
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }
}

