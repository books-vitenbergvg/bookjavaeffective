package Chapter8.Ch8_51;

public class WaitQueuePerf {
    public static void main(String[] args) {
        PingPongQueue q1 = new PingPongQueue();
        PingPongQueue q2 = new PingPongQueue();
        q1.enqueue(q2); // Запускаем систему
// Дадим системе 10 с на прогрев
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
// Подсчитаем количество переходов за 10 с
        int count = q1.count;
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
        System.out.println(q1.count - count);
        q1.stop();
        q2.stop();
    }
}
