package Chapter6.Ch6_30;

import java.util.Random;

public class Rand {
    static Random rnd = new Random();

    // Неправильно, хотя встречается часто
    static int random(int n) {
        return Math.abs(rnd.nextInt()) % n;
    }

    //Недостатки:
    //1) если n - небольшая степень числа 2, то последовательность генерируемых чисел быстро начнет повторяться.
    //2) Если n - не степень числа 2, некоторые числа будут получаться чаще других

    //ER ~500000 AR~666666
    public static void main(String[] args) {
        int n = 2 * (Integer.MAX_VALUE / 3);
        int low = 0;
        for (int i = 0; i < 1000000; i++) if (random(n) < n / 2) low++;
        System.out.println(low);

        //Правильный подход
        //int rand = Random.nextInt(1000000);//?
    }

}
