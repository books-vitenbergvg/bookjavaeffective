package Chapter6.Ch6_35;

import java.util.Arrays;
import java.util.Set;

public class Ch6_35 {
    // Опосредованное создание экземпляра с доступом
    public static void main(String[] args) {
        // Преобразует имя класса в экземпляр класса
        Class сl = null;
        try {
            сl = Class.forName(args[0]);
        } catch (ClassNotFoundException е) {
            System.err.println("Class not found.");// Класс не найден
            System.exit(1);
        }
        // Создает экземпляр класса
        Set s = null;
        try {
            s = (Set) сl.newInstance();
        } catch (IllegalAccessException е) {
            System.err.println("Class not accessible.");// Доступ к классу невозможен
            System.exit(1);
        } catch (InstantiationException е) {
            System.err.println("Class not instantiable.");// Класс не позволяет создать экземпляр
            System.exit(1);
        }

        // Проверяет набор
        s.addAll(Arrays.asList(args).subList(1, args.length - 1));
        System.out.println(s);
    }

}
