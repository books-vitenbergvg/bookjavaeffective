package Chapter6.Ch6_31;

public class MoneyCalcultions {
    // Ошибка: использование плавающей точки для денежных расчетов!
    public static void main(String[] args) {
        double funds = 1.00;
        int itemsBought = 0;
        for (double price = .10; funds >= price;
        price += .10){
            funds -= price;
            itemsBought++;
        }
        System.out.println (itemsBought + " items bought.");
        System.out.println("Change: $" + funds);
    }

}
