package Chapter5.Ch5_26;

import java.util.*;

//Ошибка: неверное использование перезагрузки!
public class CollectionClassifier {
    public static String classify(Set s) {
        return "Set";
    }

    public static String classify(List l) {
        return "List";
    }

    public static String classify(Collection c) {
        return "Unknown Collection";
    }

    //ER: Set List Unknown Collection
    //AR: Unknown Collection Unknown Collection Unknown Collection
    public static void main(String[] args) {
        Collection[] tests = new Collection[] {
                new HashSet(), //Набор
                new ArrayList(), //Список
                new HashMap().values() //Не набор и не список
        };

        for (int i = 0; i < tests.length; i++) {
            System.out.println(classify(tests[i]));
        }
    }

    //Исправление - замена единым методом, который явно проверяет тип
//    public static String classify(Collection c) {
//        return (c instanceof Set ? "Set":
//                (c instanceof List ? "List" :
//                        "Unknown Collection"));
//    }
}
