package Chapter5.Ch5_23;

import java.math.BigInteger;

public class Class5_23 {
    /**
     * Возвращает объект BigInteger, значением которого является (this mod m)
     * Всегда возвращает неотрицательное значение.
     *
     * @param m - модуль должен быть положительным числом
     * @return this mod m
     * @throws ArithmeticException, если m<=0
     */
    public BigInteger mod(BigInteger m) {
        if (m.signum() <= 0) {
            throw new ArithmeticException("Modulus not positive");
        }
        //Вычисления
        //...
        return m;
    }
}
