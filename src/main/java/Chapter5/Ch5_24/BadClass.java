package Chapter5.Ch5_24;

import java.util.Date;

public class BadClass {

    public static void main(String[] args) {
        //Первая атака на содержимое экземпляра Period
        Date start = new Date();
        Date end = new Date();
        Period p1 = new Period(start, end);
        end.setYear(78);//Изменяет содержимое объекта p!

        //Вторая атака на содержимое экземпляра Period
        Period p2 = new Period(start, end);
        p2.end().setYear(78);//Изменяет внутренние данные p!
    }
}
