package Chapter5.Ch5_24;

import java.util.Date;

//Неправильный класс "неизменяемого" периода времени
public class Period {
    private final Date start;
    private final Date end;

    /**
     * @param start - начало периода
     * @param end   - конец периода, не должен предшествовать началу.
     * @throws IllegalArgumentException, если start позже, чем end.
     * @throws NullPointerException,     если start или end равен null.
     */
    public Period(Date start, Date end) {
        if (start.compareTo(end) > 0) {
            throw new IllegalArgumentException(start + " after " + end);
        }

        this.start = start;
        this.end = end;
    }

    public Date start() {
        return start;
    }

    public Date end() {
        return end;
    }
    //...

    //Для защиты от первой атаки:
    //Исправленный конструктор создает резервные копии для параметров
//    public Period(Date start, Date end) {
//        this.start = new Date(start.getTime());
//        this.end = new Date(end.getTime());
//
//        if (this.start.compareTo(this.end) > 0) {
//            throw new IllegalArgumentException(start+ " after " + end);
//        }
//    }

    //Для защиты от второй атаки:
    //Исправленные методы доступа (создаются резервные копии внутренних полей)
//    public Date start() {
//        return (Date) start.clone();
//    }
//
//    public Date end() {
//        return (Date) end.clone();
//    }
}
