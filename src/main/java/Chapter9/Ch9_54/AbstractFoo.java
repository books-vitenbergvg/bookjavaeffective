package Chapter9.Ch9_54;

//Для несереализуемого класса, который прдназначен для наследования, необходимо рассмотреть возможность создания конструктора без параметров
public abstract class AbstractFoo {
    private int x, y; // Состояние
    private boolean initialized = false;

    public AbstractFoo(int x, int y) {
        initialize(x, y);
    }

    /**
     * Данный конструктор и следующий за ним метод позволяют методу
     * readObject в подклассе инициализировать наше внутреннее состояние.
     */
    protected AbstractFoo() {
    }

    protected final void initialize(int x, int y) {
        if (initialized)
            throw new IllegalStateException(
                    "Already initialized");
        this.x = x;
        this.y = y;
        // Делает все остальное, что делал прежний конструктор
        initialized = true;
    }

    /**
     * Эти методы предоставляют доступ к внутреннему состоянию класса,
     * и потому его можно сериализовать вручную, используя
     * метод writeObject из подкласса
     */
    protected final int getX() {
        return x;
    }

    protected final int getY() {
        return y;
    }

    // Должен вызываться для всех открытых методов экземпляра
    private void checkInit() throws IllegalStateException {
        if (!initialized)
            throw new IllegalStateException("Uninitialized");
    }
    // Остальное опущено
}
