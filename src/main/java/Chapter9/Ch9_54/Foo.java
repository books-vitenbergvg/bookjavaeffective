package Chapter9.Ch9_54;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Foo extends AbstractFoo implements Serializable {
    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        s.defaultReadObject();

        //Ручная десериализация и инициализация состояния суперкласса
        int x = s.readInt();
        int y = s.readInt();
        initialize(x, y);
    }

    private void writeObject(ObjectOutputStream s)
            throws IOException {
        s.defaultWriteObject();

        //Ручная сериализация состояния суперкласса
        s.writeInt(getX());
        s.writeInt(getY());
    }

    //Конструктор не использует никаких причудливых механизмов
    public Foo(int x, int y) {
        super(x, y);
    }
}