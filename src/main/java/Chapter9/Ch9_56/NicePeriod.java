package Chapter9.Ch9_56;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;

public class NicePeriod implements Serializable {
    private final Date start;
    private final Date end;

    /**
     * @param start - начало периода
     * @param end   - конец периода, не должен предшествовать началу периода
     * @throws IllegalArgumentException – если начало периода указано после конца
     * @throws NullPointerException     – если начало или конец периода нулевые
     */
    public NicePeriod(Date start, Date end) {
        this.start = new Date(start.getTime());
        this.end = new Date(end.getTime());
        if (this.start.compareTo(this.end) > 0)
            throw new IllegalArgumentException(start + " > " + end);
    }

    public Date start() {
        return (Date) start.clone();
    }

    public Date end() {
        return (Date) end.clone();
    }

    public String toString() {
        return start + " - " + end;
    }
    //Остальное опущено

    //Для решения этой проблемы создадим в классе Period методreadObject, который будет вызывать
    //defaultReadObject и проверять правильность десериализованного объекта.
    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        //Проверим правильность инвариантов
        if (start.compareTo(end) > 0)
            throw new InvalidObjectException(start + " after " + end);
    }
}
