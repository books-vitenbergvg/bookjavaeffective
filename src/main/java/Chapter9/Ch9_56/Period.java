package Chapter9.Ch9_56;

import java.io.Serializable;
import java.util.Date;

//Неизменяемый класс, который использует резервное копирование
public final class Period implements Serializable {
    private final Date start;
    private final Date end;

    /**
     * @param start - начало периода
     * @param end   - конец периода, не должен предшествовать началу периода
     * @throws IllegalArgumentException – если начало периода указано после конца
     * @throws NullPointerException     – если начало или конец периода нулевые
     */
    public Period(Date start, Date end) {
        this.start = new Date(start.getTime());
        this.end = new Date(end.getTime());
        if (this.start.compareTo(this.end) > 0)
            throw new IllegalArgumentException(start + " > " + end);
    }

    public Date start() {
        return (Date) start.clone();
    }

    public Date end() {
        return (Date) end.clone();
    }

    public String toString() {
        return start + " - " + end;
    }
    //Остальное опущено
}
