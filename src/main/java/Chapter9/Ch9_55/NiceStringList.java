package Chapter9.Ch9_55;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//Правильная сериализованная форма для класса StringList - это количество строк в списке, за которым следуют сами строки.
//Исправленный вариант StringList содержит методы writeObject и readObject, которые реализуют правильную сериализованную форму.
//Модификатор transient указывает на то, что экземпляр поля должен быть исключен из сериализованной формы, применяемой по умолчанию
// Класс StringList справильной сериализованной формой
public class NiceStringList implements Serializable {
    private transient int size = 0;
    private transient Entry head = null;

    // Больше нет реализации Serializable
    private static class Entry {
        String data;
        Entry next;
        Entry previous;
    }

    // Добавляет указанную строку в конец списка
    public void add(String s) {
        //...
    }

    /**
     * Сериализует данный экземпля <tt>StringList</tt>.
     *
     * @serialData Показывается размер списка (количество
     * содержащихся в нем строк (<tt>int</tt», за которым* в правильной последовательности
     * следуют все элементы списка (каждый в виде <tt>String</tt>).
     */
    private void writeObject(ObjectOutputStream s)
            throws IOException {
        s.defaultWriteObject();
        s.writeInt(size);
        //Выписываем все элементы в правильном порядке
        for (Entry e = head; e != null; e = e.next)
            s.writeObject(e.data);
    }

    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        int size = s.readInt();
        //Считываем все элементы и вставляем
        for (int i = 0; i < size; i++)
            add((String) s.readObject());
    }
//Остальное опускаем
}