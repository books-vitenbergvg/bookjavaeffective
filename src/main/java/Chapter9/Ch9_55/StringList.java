package Chapter9.Ch9_55;

import java.io.Serializable;

//Ужасный кандидат на использование сериализованной формы, предлагаемой по умолчанию

//Логически этот класс представляет последовательность строк.
//Физически последовательность представлена им как двусвязный список. Если вы примите сериализованную форму,
// предлагаемую по умолчанию, она старательно отразит каждый элемент в этом связном списке, а также
//все связи между этими элементами в обоих направлениях.
public class StringList implements Serializable {
    private int size = 0;
    private Entry head = null;

    private static class Entry implements Serializable {
        String data;
        Entry next;
        Entry previous;
    }
    //Остальное опущено
}