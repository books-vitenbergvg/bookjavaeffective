package Chapter9.Ch9_55;

import java.io.Serializable;

//Хороший кандидат для использования формы, предлагаемой по умолчанию
public class Name implements Serializable {
    /**
     * Фамилия. Не должна быть пустой (non-null)
     *
     * @serial
     */
    private String lastName;
    /**
     * Имя. Не должно быть пустым.
     * '@serial
     */
    private String firstName;
    /**
     * Отчество или '\u0000', если инициал отсутствует
     *
     * @serial
     */
    private char middlelnitial;

    //Остальное опущено
}